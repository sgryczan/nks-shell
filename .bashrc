sleep 2
cd ~
figlet -c "NKS-Shell"
echo "*************************"
echo "Welcome to the NKS Shell!"
echo "*************************"
echo ""
echo "Configuring NKS CLI...."
. /etc/profile.d/bash_completion.sh
. <(nks version --generatecompletion)
nks version
echo ""

echo "Run 'nks -h for a list of commands.'"