FROM hashicorp/terraform:0.11.13

RUN apk add --no-cache \
    curl \
    bash \
    zsh \
    bash-completion \
    jq \
    unzip \
    python \
    zip \
    go \
    make \
    musl-dev \
    openssl \
    gcc \
    ca-certificates \
    figlet 

# Install kubectl
# Note: Latest version may be found on:
# https://aur.archlinux.org/packages/kubectl-bin/
ADD https://storage.googleapis.com/kubernetes-release/release/v1.15.0/bin/linux/amd64/kubectl /usr/local/bin/kubectl

# Install kubectl
RUN \
    chmod +x /usr/local/bin/kubectl && \
    mkdir ~/.kube && \
    kubectl version --client

# Install Helm
RUN \
    wget https://get.helm.sh/helm-v2.12.1-linux-amd64.tar.gz && \
    tar xvzf helm-v2.12.1-linux-amd64.tar.gz && \
    rm helm-v2.12.1-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/ && \
    helm init --client-only


#ENV GOPATH="/root/go"
ENV PATH="/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Install newer Golang
RUN \
    wget -O go.tgz https://dl.google.com/go/go1.13.src.tar.gz &&\
    tar -C /usr/local -xzf go.tgz && \
    rm go.tgz && \
    cd /usr/local/go/src && \
    ./make.bash

   
 
RUN adduser -s /bin/zsh -S nks
USER nks

ENV GOPATH="/home/nks/go"
ENV PATH="/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$GOPATH/bin"

RUN \
    wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh

RUN \
    mkdir ~/.kube

RUN \
    go get -u github.com/NetApp/terraform-provider-nks && \
    go get -u github.com/yudai/gotty

# Install nks terraform provider
RUN cd $GOPATH/src/github.com/NetApp/terraform-provider-nks && \
    rm go.mod go.sum && make build && \
    mkdir -p ~/.terraform.d/plugins && \
    mv ~/go/bin/terraform-provider-nks ~/.terraform.d/plugins

RUN \
    mv $GOPATH/bin/gotty $GOPATH/bin/entrypoint

RUN \
    go get -u gitlab.com/sgryczan/nks-cli/nks

# Build NKS from dev branch
#RUN \
#    cd $GOPATH/src && \
#    mkdir -p gitlab.com/sgryczan 
#
#RUN \ 
#    cd $GOPATH/src/gitlab.com/sgryczan && \
#    git clone --single-branch --branch dev https://gitlab.com/sgryczan/nks-cli && \
#    cd nks-cli/nks && \
#    make deps && make && \
#    mv nks $GOPATH/bin

#COPY nks_unix $GOPATH/bin/nks
COPY .bashrc  /home/nks/.bashrc
COPY .zshrc /home/nks/.zshrc

COPY config /home/nks/.gotty
COPY ssl/cert.pem /home/nks/.gotty.crt
COPY ssl/key.pem /home/nks/.gotty.key

COPY index.html /home/nks/index.html

ENTRYPOINT [ "entrypoint" ]